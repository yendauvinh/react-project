import {Component} from '@angular/core';

@Component({
    selector : 'my-app',
    templateUrl : 'component-demo/app.component.html'
})

export class AppComponent{
    appTitle: string = 'Welcome';
}